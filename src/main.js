import Vue from 'vue';
import App from './App.vue';
import jQuery from 'jquery';
import 'popper.js';
import 'bootstrap';
import './assets/app.scss'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faUserSecret } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import router from './routes';

new Vue({ el: '#app', router, render: h => h(App) })