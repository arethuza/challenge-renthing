import Vue from 'vue'
import Router from 'vue-router'
import Home from './pages/Home.vue';
import Detail from './pages/DetailFilm.vue';
import Daftar from './pages/Daftar.vue';
import Wish from './pages/Wish.vue';

Vue.use(Router)

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            component: Home
        },
        {
            path: '/daftar',
            component: Daftar,
        },
        {
            path: '/detail/:id',
            component: Detail,
        },
        {
            path: '/wish',
            component: Wish,
        },
    ]
})